﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    public GameObject Player;
    private Vector3 Offset;

	// Use this for initialization
	void Start ()
    {
        Offset = transform.position - Player.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    //Runs every frame after all items have been processed in update.
    private void LateUpdate()
    {
        transform.position = Player.transform.position + Offset;
    }
}
