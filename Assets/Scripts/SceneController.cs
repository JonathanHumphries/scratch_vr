﻿using UnityEngine;
using System.Collections.Generic;
using Assets.ScrollEngineImplementation;
using UnityEngine.UI;
using WeBuildThings.ScrollEngine;

public class SceneController : MonoBehaviour
{

    public GameObject DirectoryObject;
    public Text MessageText;
    public HashSet<GameObject> GameObjects;
    private ScrollEngine GameHub;
    public GameObject PlayerAvatar;

	// Use this for initialization
	void Start () {
        GameObjects = new HashSet<GameObject>();
        var commandProcessor = new CommandProcessor(DirectoryObject, MessageText);
        GameHub = new ScrollEngine(commandProcessor);
        GameHub.Start();       
	}

	// Update is called once per frame
	void Update ()
    {
        ICommandExecutionParameters commandParameters = new MyCommandParameters(PlayerAvatar.transform);
        GameHub.Update(commandParameters);
	}

    private void FixedUpdate()
    {
        GameHub.FixedUpdate();
    }
}
