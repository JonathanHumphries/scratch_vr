﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;

public class PlayerController : MonoBehaviour
{
    public float Speed;
    private Rigidbody RigidBody;
    public Text MessageText;
    public Text AvatarText;

    public GameObject DirectoryObject;


    private void Start()
    {
        RigidBody = this.GetComponent<Rigidbody>();
    }


    //Update is called when rendering a frame
    /*
    private void Update()
    {
        
    }
    */

    //Fixed update is called when physics is called
    private void FixedUpdate()
    {
        ProcessInput();

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        var movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        RigidBody.AddForce(movement * Speed);

    }

   

    private void ProcessInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            RigidBody.velocity = Vector3.zero;
            RigidBody.angularVelocity = Vector3.zero;
        }
    }

   

   

}