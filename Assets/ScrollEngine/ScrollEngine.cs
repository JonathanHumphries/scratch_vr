﻿using UnityEngine;
using System.Collections;

namespace WeBuildThings.ScrollEngine
{
    public class ScrollEngine
    {
        public ICommandInterface CommandProcessor { get; set; }

        public ScrollEngine(ICommandInterface commandProcessor)
        {
            CommandProcessor = commandProcessor;
        }

        // Use this for initialization
        public void Start()
        {

        }

        // Update is called once per frame
        public void Update(ICommandExecutionParameters commandParameters)
        {
            if (CommandProcessor != null) ProcessUserCommands(commandParameters);
        }

        public void FixedUpdate()
        {

        }

        private void ProcessUserCommands(ICommandExecutionParameters commandParameters)
        {
            bool found = false;
            foreach (var command in CommandProcessor.ControlCommandMap)
            {
                if (command.CheckInput(commandParameters))
                {
                    found = true;
                    break;
                }
            }
            if (found)
            {
                foreach (var command in CommandProcessor.ControlCommandMap)
                {
                    command.Clear();
                }
            }
        }
    }

}