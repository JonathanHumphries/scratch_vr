﻿namespace WeBuildThings.ScrollEngine
{
    public interface IControlInterface
    {
        bool CheckInput(ICommandExecutionParameters commandExecutionParameters);
        void Clear();
    }
}
