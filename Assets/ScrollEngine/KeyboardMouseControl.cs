﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace WeBuildThings.ScrollEngine
{
    public class KeyboardMouseControl : IControlInterface
    {
        public KeyboardMouseCommandType Key1Type;
        public KeyboardMouseCommandType Key2Type;
        public KeyboardMouseCommandType Mouse1Type;
        public KeyCode Key1;
        public KeyCode Key2;
        public ExecuteCommand CommandToExecute;

        private bool PreviousKey1;
        private bool PreviousKey2;
        private bool PreviousMouse0;
        private bool PreviousMouse1;

        public KeyboardMouseControl(ExecuteCommand commandToExecute)
        {
            CommandToExecute = commandToExecute;
        }

        public bool CheckInput(ICommandExecutionParameters commandParameters)
        {
            bool result = false;
            bool param1 = false;
            bool param2 = false;
            bool param3 = false;
            if ((Key1Type == KeyboardMouseCommandType.KeyPressed && Input.GetKeyDown(Key1)) || Key1Type == KeyboardMouseCommandType.NoControlSet) param1 = true;
            else if ((Key1Type == KeyboardMouseCommandType.KeyPressedThenReleased && !Input.GetKeyDown(Key1) && PreviousKey1) || Key1 == KeyCode.None) param1 = true;
            if ((Key2Type == KeyboardMouseCommandType.KeyPressed && Input.GetKeyDown(Key2)) || Key2Type == KeyboardMouseCommandType.NoControlSet) param2 = true;
            else if ((Key2Type == KeyboardMouseCommandType.KeyPressedThenReleased && !Input.GetKeyDown(Key2) && PreviousKey2) || Key2 == KeyCode.None) param2 = true;
            if (Mouse1Type == KeyboardMouseCommandType.NoControlSet) param3 = true;
            else if (Mouse1Type == KeyboardMouseCommandType.LeftButtonPressed && Input.GetMouseButtonDown(0))
            {
                param3 = true;
            }
            else if (Mouse1Type == KeyboardMouseCommandType.LeftButtonPressedThenReleased && Input.GetMouseButtonUp(0) && PreviousMouse0)
            {
                param3 = true;
            }
            else if (Mouse1Type == KeyboardMouseCommandType.RightButtonPressed && Input.GetMouseButtonDown(1))
            {
                param3 = true;
            }
            else if (Mouse1Type == KeyboardMouseCommandType.RightButtonPressedThenReleased && Input.GetMouseButtonUp(1) && PreviousMouse1)
            {
                param3 = true;
            }
            if (param1 && param2 && param3)
            {
                result = true;
                Debug.Log("Executing: " + CommandToExecute.Target.ToString() + "."+ CommandToExecute.Method.Name);
                CommandToExecute(commandParameters);
            }
            PreviousKey1 = Input.GetKeyDown(Key1);
            PreviousKey2 = Input.GetKeyDown(Key2);
            PreviousMouse0 = Input.GetMouseButtonDown(0);
            PreviousMouse1 = Input.GetMouseButtonDown(1);
            return result;
        }

        public void Clear()
        {
            PreviousKey1 = false;
            PreviousKey2 = false;
            PreviousMouse0 = false;
            PreviousMouse1 = false;
        }
    }
}
