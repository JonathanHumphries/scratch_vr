﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.ScrollEngine
{
    public enum KeyboardMouseCommandType
    {
        NoControlSet,
        KeyPressed,
        KeyPressedThenReleased,
        KeyReleased,
        LeftButtonPressed,
        LeftButtonPressedThenReleased,
        RightButtonPressed,
        RightButtonPressedThenReleased
    };
}
