﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace WeBuildThings.ScrollEngine
{
    public delegate void ExecuteCommand(ICommandExecutionParameters commandParameters);

    public interface ICommandInterface
    {
        HashSet<IControlInterface> ControlCommandMap { get; set; }
    }


}