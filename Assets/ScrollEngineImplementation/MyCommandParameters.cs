﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using WeBuildThings.ScrollEngine;

namespace Assets.ScrollEngineImplementation
{
    public class MyCommandParameters: ICommandExecutionParameters
    {
        public MyCommandParameters(Transform transform)
        {
            Transform = transform;
        }

        public Transform Transform { get; set; }
    }
}
