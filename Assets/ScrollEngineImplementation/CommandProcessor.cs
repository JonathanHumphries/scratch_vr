﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using WeBuildThings.ScrollEngine;

namespace Assets.ScrollEngineImplementation
{
    public class CommandProcessor : ICommandInterface
    {
        private bool DisplayDirectoriesOn;
        public GameObject DirectoryObject;
        private HashSet<DirStruct> Directories;
        public Text MessageText;

        public HashSet<IControlInterface> ControlCommandMap { get; set; }

        public CommandProcessor(GameObject directoryObject, Text messageText)
        {
            DirectoryObject = directoryObject;
            MessageText = messageText;
            ControlCommandMap = new HashSet<IControlInterface>();
            var displayDirectoriesControl = new KeyboardMouseControl(OnDisplayDirectories) { Key1Type = KeyboardMouseCommandType.KeyPressedThenReleased, Key1 = KeyCode.F };
            ControlCommandMap.Add(displayDirectoriesControl);

        }

        public void OnDisplayDirectories(ICommandExecutionParameters commandParameters)
        {
            var transform = ((MyCommandParameters)commandParameters).Transform;
            DisplayDirectoriesOn = !DisplayDirectoriesOn;
            if (DisplayDirectoriesOn)
            {
                MessageText.text += "Displaying Directories";
                DisplayDirectories(transform);
            }
            else
            {
                DestroyDirectories();
            }
        }

        private void DestroyDirectories()
        {
            if (Directories != null)
            {
                foreach (var item in Directories)
                {
                    UnityEngine.Object.Destroy(item.GameObject);
                }
                Directories = new HashSet<DirStruct>();
            }
        }

        private void DisplayDirectories(Transform transform)
        {
            if (Directories == null)
            {
                Directories = new HashSet<DirStruct>();
            }

            float xOffSet = 0;
            float x = transform.position.x;
            float z = transform.position.z + 4;
            bool leftRight = false;
            var directories = new DirectoryInfo(Environment.CurrentDirectory).GetDirectories();
            for (int i = 0; i < directories.Count(); i++)
            {
                GameObject newObject = (GameObject)UnityEngine.Object.Instantiate(DirectoryObject, new Vector3(x, 0.5F, z), Quaternion.identity);
                Directories.Add(new DirStruct(directories[i], newObject));
                if (newObject.transform.childCount >= 2)
                {
                    var childTransform = newObject.transform.GetChild(1);
                    var textComponent = childTransform.GetComponent<TextMesh>();
                    textComponent.text = directories[i].Name;
                }
                if (i % 2 == 0) xOffSet++;
                leftRight = !leftRight;
                x = transform.position.x + ((leftRight) ? xOffSet * 2.0F : -xOffSet * 2.0F);
            }
        }

        private struct DirStruct
        {
            public DirectoryInfo Directory;
            public GameObject GameObject;

            public DirStruct(DirectoryInfo directory, GameObject gameObject)
            {
                Directory = directory;
                GameObject = gameObject;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Directory"))
            {
                other.gameObject.SetActive(false);
            }
        }

    }
}
